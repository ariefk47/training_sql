def create_table(db=None, sql_query=None):
    cursor = db.cursor()
    cursor.execute("DROP TABLE IF EXISTS SISWA")

    sql = sql_query
    cursor.execute(sql)

    return db.close()
